%% IMPLEMENTS APIS to estimate a linear control u=Ax+b and smoothing distribution p(x_{0:T}|y_{0:T}) recursively
%  APIS needs to be included in a script where the observations and model
%  parameters are specified (cf. example.m)
% This script implements a quadratic observation cost directly, meaning
% that the observation model is Gaussian directly on top of the state. (A
% more general observation model needs to be added...)
%  This script implements a rejection sampling that displays a message
%  'Sampling rejected' whenever the effective sample size ESS is lower than
%   2*ESSstop/3 at the end of a maximal number of iterations and gives the value 1 to the variable FAILED.
%  To avoid a low ESS in the bootstrap, an adaptive importance
%   sampling at t=0 is implemented as a Gaussian with the smoothing
%   statistics at time t=0 obtained from the previous interation.
%   It has no KDE for the posterior p(x_0|y_{0:T}) because the matlab function is too slow; but this could be
%   implemented (more efficiently?)

% clear all
% load('debugg_data')

%% CHANGES NEEDED:
%   Cost Function (Cost of Observations) in its general form needs to be added
%   A lighter version with less memory requirements and optimized

%% Functions required:
%       Prior.m : Samples initialization of particles
%       Drift.m: Propagates the particles acording to a stochastic process
%       diffusion.m : value 1 if noise is constant; function of the state if the model has state-dependent noise  

%% INPUT is given in the script containing this APIS.m. It needs to contain:
% Options
%       extra='cost' : outputs extra data e.g.  E(S) and <S> over iterations

% Parameters for the algorithm
%       N  : # of particles 
%       iters : maximal number of iterations (stop criterion if ESS doesn't improve)
%       ESSstop: Threshold to stop the iterations
%       theta  : threshold for %ESS below which only open loop controller
%                   is updated
%       learning_rate : learning rate for the control update

% For initialization of particles
%       mean_prior : mean of prior as vector
%       std_prior : standard deviation of prior as vector/matrix
%       pri : is a string specifying the prior 
%               (e.g. pri='wide' in Prior.m implements a wide Gaussian
%               initialization of the particles)
%       pos : is a string specifying the initialization of particles for
%               k>1 iterations; if pos='adapt' it implements the
%               initialisation taking statistics of the postertior at t=0

%Data and system integration :
%       Yx : Observations
%       obs: Time stamp of observations
%       yvar : Variance of the observation
%  dim_obs: # of dimensions observed
     dim_obs=size(Yx,1);
%       sys : is a string flag spacifying the system (affects Drift.m and Prior.m)
%       T : # of integrations time steps
%       h :   time increments for integration. Defined by h=Time/T
%       param : parameters of the model (a vector along 2nd dim)
%       D : # of dimensions of (controlled) system states D=length(X(:,t));
%       dyn_var : noise parameter of the dynamic s.t. sigmaD=var_dyn*h is
%                  the variance of the dynamics
if length(dyn_var)==1
    dyn_var = dyn_var*ones(1,D);
end
R=1./dyn_var;  
    
%% OUTPUT:
%       dW  : NxTxD matrix with noise increments
%       wend   : N-vector with unnormalized weights
%       PS : NxTxD array with sheets being NxT matrices for each dimension
%       PSESS : percentage of ESS of particle smoother
        iter_maxESS=nan;
%Optional:
    %For Priors:
%       mS0_iters : mean of path cost from particle ensamble in each iteration
%       stdS0_iters : standard deviation of path cost in each iteration
%       SIGMA : covariance matrix of particle system at t=0
    %Cost:
%       St : time development of cost per particle
%       wmS0_iter : weighted mean of path cost
%       wstdS0_iter : weighted standard deviation of path cost
%       mS0_iter : mean of particle representation
%       stdS0_iter : standard deviation of particle representation
    T=int64(T);
    PS=zeros(N,D,T); %Contains smoothing particle system
    wend = zeros(N,1);      %weight of each particle
    PSESS=zeros(1,iters); %ESS of the smoothing distro
    mSD=zeros(T,D);       % saves mean of smoothing distribution 
    varSD=ones(T,D);
    Obs_Signal=zeros(N,dim_obs,T);%([N,size(Yx')]);
    Lambda=zeros(1,iters); 
%Control parameters (linear feedback controller)
    A=zeros(D,D,T);
    b=zeros(D,T);

switch extra
    case 'cost'
% E(S) and <S> over iterations
    mSend_iter = zeros(1,iters);
    stdSend_iter = zeros(1,iters);
    wmSend_iter = zeros(1,iters);
    wstdSend_iter = zeros(1,iters);
end

%% Iteration for the calculation of control U
% To control algorithm
k=1;
psESS=0;
FAILED=0;
activate = 0;
%% SAMPLE from Prior.m to start particles for first iteration
        PS(:,:,1)=Prior(pri,mean_prior,std_prior,N,dim_obs);

%% Iterations
while psESS/N < ESSstop 

%INTERNAL PARAMETERS for each iteration
    V = zeros(N,1);         % STATE Cost per particle
    Ucost = zeros(N,1);  % CONTROL COST per particle
    S0=0;                       % Cost of initializaton w. IS 
%     St = zeros(N,T);        % Cost of particles over time
    
%% Initialize particles
if k>1 && strcmp(pos,'wide')
    PS(:,:,1)=Prior(pri,mean_prior,std_prior,N,dim_obs);
elseif k>1 && strcmp(pos,'adapt')
    if var_SD0==0
        disp('var_SD0 = 0 !')
        var_SD0 = std_prior.^2;
    end
    PS(:,:,1)=Prior(pos,mean_SD0,sqrt(var_SD0),N,dim_obs);
    S0 = + (0.5./repmat(var_SD0,N,1)).*(repmat(mean_SD0,N,1)-squeeze(PS(:,:,1))).^2 -(0.5./repmat(std_prior.^2,N,1)).*(repmat(mean_prior,N,1)-squeeze(PS(:,:,1))).^2;
    S0 = sum(S0,2);
    S0 = S0 - max(S0);
elseif k>1 && strcmp(pos,'adapt_mvn')
    PS(:,:,1)=Prior(pos,mean_SD0,SIGMA,N,dim_obs);
    S0 = 0.5*(repmat(mean_SD0,N,1)-squeeze(PS(:,:,1))).*((repmat(mean_SD0,N,1)-squeeze(PS(:,:,1)))/SIGMA) -(0.5./repmat(std_prior.^2,N,1)).*(repmat(mean_prior,N,1)-squeeze(PS(:,:,1))).^2;
    S0 = sum(S0,2);
    S0 = S0 - max(S0);
elseif k>1 && ~strcmp(pos,'fixed')
    error('Sampling interrupted: Posterior sampling for higher itarations not specified')
end

    % Noise increment
       dW = zeros(N,D,T); % define dW without sqrt(dyn_var) because below <dW/ds> is defined with dyn_var (cf. JSP paper, Thm. 1)
       
%% Forward sampling
     ot=1; %counts observations  
for t=1:T
          
       % observation signal
           Obs_Signal(:,:,t) = ObSignal(PS(:,:,t),param_signal,signal_type);
       %Add observation cost
       if  ot<=numel(obs) && t==obs(ot) 
           %COST
               V = V + logLikelihood(Yx(ot),Obs_Signal(:,:,t),param_obs,obs_model) ;% Gaussian observ. model: - (0.5/yvar)*(Yx(ot)-Obs_Signal(:,ot,1)).^2; 
               ot=ot+1;
       end

      if t<=T-1 %Propagate particles
 
          %Calculate Control
          PSt=bsxfun(@minus, PS(:,:,t), mSD(t,:));%PS(:,:,t)-repmat(mSD(t,:),N,1); %
          PSt=PSt./repmat(sqrt(varSD(t,:)),N,1); %bsxfun(@rdivide, PSt, sqrt(varSD(t,:)));%
          At = A(:,:,t);
          bt = b(:,t);

          Ut = At*PSt' + repmat(bt,1,N); %Linear feedback controller bsxfun(@plus,At*PSt',bt);%
          Ut=Ut';
          if isnan(sum(sum(Ut)))
              error('Control step is NAN!')
          end
          %Step Forward
          dW(:,:,t) = randn(N,D)*sqrt(h);

          %Euler Method    
          PS(:,:,t+1) = PS(:,:,t) + h*Drift(t,PS(:,:,t),param,sys) + diffusion(PS(:,:,t),diff_case)*sqrt(diag(dyn_var)).*(Ut*h + dW(:,:,t)); %
          switch sys
              case 'DCM'
               PS(:,3,t+1) = max(PS(:,3,t+1) ,0);    
          end
          
           Ucost = Ucost - Ut.*(0.5*h*Ut + dW(:,:,t))*R';
           
           if sum(sum(sum(isnan(PS(:,:,t+1)))))
               error('Particle System IS NAN!')
           end

      end
      
end % Loop over time t ends

S = V + Ucost;
switch sys
    case 'DCM'
        indices_fneg = find(sum(PS(:,3,:)==0,3)>2); 
        if ~isempty(indices_fneg)
            S(indices_fneg) = -Inf;
            fprintf('%i Particles were killed!',length(indices_fneg))
        end
end
%% Compute psESS:
%compute weights
Smax=max(S);
uw = exp(((S-Smax)+S0));
  % Normalize weights
    wend = uw/sum(uw);  
           if sum(isnan(wend))
               error('Weights are NAN!')
           end
    % ESS of the smoothing distribution
    psESS = 1/sum(wend.^2);   
    ESS_raw(k) = psESS/N;
 
% tic
lambda=1;
while psESS/N < gamma
    if lambda > 1000
        fprintf('Annealing unsuccessful')
        FAILED=1
        break
    end
lambda=beta*lambda;
S = (S0 + V + Ucost)/lambda; % R=1/dyn_var so it only holds for lambda = 1
Smax=max(S);
uw = exp(S-Smax);
  % Normalize weights
    wend = uw/sum(uw);  
    % ESS of the smoothing distribution
    psESS = 1/sum(wend.^2);  
    fprintf('*')
end
% anneal_time(k)=toc;
Lambda(k)=lambda;
% Final ESS and Weights
PSESS(k) = psESS/N;
diagW=spdiags(wend,0,N,N);
%
if ESSstop>1 && ~activate &&  max(PSESS) > 0.2
    activate = 1;
    start_active = k;
end
    %% Interesting measures
    switch extra
        case 'cost'
            % Expected End Cost per iteration E_u[S(0)]
            mSend_iter(k) = mean(S);
            stdSend_iter(k) = sqrt(var(S));
            % Expected End Cost per iteration under opt control <S(0)>=E_u*[S(0)]
            wmSend_iter(k) = wend'*S;
            wstdSend_iter(k) = sqrt(var(S,wend));
    end
    
    switch pos
        case 'adapt'
            %Posterior at t=0: values for KDE initialization
            mean_SD0 = wend'*PS(:,:,1);
            var_SD0=var(PS(:,:,1),wend);
        case 'adapt_mvn'
            %Posterior at t=0: values for KDE initialization
            mean_SD0 = wend'*PS(:,:,1);
            SIGMA = (PS(:,:,1)-mean_SD0)'*diagW*(PS(:,:,1)-mean_SD0);
    end

    %% Values for representing the smoothing distribution
    % Statistics of smoothing distribution
    %<x> in NxT-matrix form
    mB = zeros(N,D,T);
    for d=1:D
        mSD(:,d) = wend'*squeeze(PS(:,d,:));
        mB(:,d,:) = repmat(mSD(:,d)',N,1,1);
    end

%% Define basis:
  B = (PS - mB);
  
%% Calculate CONTROL parameters at each time step     
     if strcmp(pri,'fixed')
         start_t=2;
     else
         start_t=1;
     end
    
%     disp('WITHOUT parfor')
    parfor t=start_t:T %for 'fixed' case start at t=2 and afterwards define A(1)=A(2) and b(1)=b(2) or the mean across a small window
        %% Solve the linear system H*dA = dW_x for each time t
        dWt=dW(:,:,t)/h;% dW is defined with sqrt(diag(dyn_var)*h) during integration
        pi0 = wend'*dWt;
        if psESS < 10    
            %UPDATE of control parameters
            b(:,t) = b(:,t) + learning_rate*pi0';
        else
            Bt=B(:,:,t);
            BtW=Bt'*diagW;
            hh=BtW*Bt;
            varSD(t,:)=diag(hh);
            norm=diag(1./sqrt(varSD(t,:)));
            hh=norm*hh*norm;
            %     Condition(k,t)=cond(hh); %To check for numerical stability
            dW_x = BtW*dWt; % is (K:# of basis functions)xD
            dW_x = norm*dW_x;
            %%Solve control equation
            dAmat = linsolve(hh,dW_x);
            %UPDATE of control parameters
            b(:,t) = b(:,t) + learning_rate*pi0';
            A(:,:,t) = A(:,:,t) + learning_rate*dAmat';
        end
        
    end

    % Extrapolate the control at the beginning for 'fixed' prior
    if strcmp(pri,'fixed')
     A(:,:,1) = 0.5*A(:,:,2) + 0.5*A(:,:,3) ;
        b(:,1) =  0.5*b(:,2) + 0.5*b(:,3);
    end
    
    %Catch the best estimation
     if PSESS(k)>1.5*gamma && max(PSESS)==PSESS(k)
        mPS= mSD;
        varPS=varSD;
        iter_maxESS=find(PSESS==max(PSESS(k:end)));
     end
    
    if activate && k > start_active + 100
         if abs(mean(PSESS(start_active:start_active+20))-mean(PSESS(k-20:k))) < 0.005
             k_end = k;
             break
         else
             start_active = start_active + 10;
         end
    end
    %count iters or break
    if k<iters
        if mod(k,100)==0
            fprintf('iter %i with ESS: %1.2g  \n',k,PSESS(k))
        elseif mod(k,2)==0
             fprintf('.')
        end
         k=k+1;
    elseif  k==iters && max(PSESS) < 3*gamma 
        disp('Sampling rejected')
        FAILED=1
        break
    else 
        k=k+1;
        disp('Threshold not achieved')
        break
    end 
    
end % while loop ends
% toc

% plotsAPIS