%DRIFT fuction
%Gives the drift term in a stachastic process

% INPUT: 
%          x : Nxdim (dim: dimension of system) matrix containing in the
%          first dimension the particles and in the second the components
%          of the state
%          param : parameters of the dynamic system

function Drift=Drift(t,x,param,sys)

[N,dim]=size(x);
Drift=zeros(N,dim);

switch sys
    case 'DCM' %DCM for a single node
        %Param of DCM
        % param_Z = [t_on,t_off,A,B,C];
        % param_HD = [epsilon,tau_s,tau_f];
        % param_Balloon = [tau_0,alpha,E_0];
        % param = [param_Z,param_HD,param_Balloon];
        % x = [z,s,f,v,q];
    %Input
        Inp = @(t) (t>param(1))*(t<param(2));
     %Neural activity
       Drift(:,1) =  (param(3)+Inp(t)*param(4))*x(:,1) + param(5)*Inp(t) ;
     %Hemodynamic equations
       Drift(:,2) = param(6)*x(:,1) - (1/param(7))*x(:,2) - (1/param(8))*(x(:,3)-1) ;
       Drift(:,3) = x(:,2);
    %Balloon model
       Drift(:,4) = ( x(:,3) - abs(x(:,4)).^param(10) )/param(9) ;
        exponent = 1./abs(x(:,3));
        E_f = ( 1 - (1-param(11)).^exponent ) /param(11);  
        Drift(:,5) = ( x(:,3).*E_f - (abs(x(:,4)).^(param(10)-1)).*x(:,5) )/param(9);
     %TESTS
        if isnan(sum(E_f*0))
            error('E_f is NAN!')  
        elseif ~isreal(Drift)
              error('Complex values!')
        elseif isnan(sum(sum(Drift)))
              disp('Drift is NAN!!')  
        end
        %TRANSFORMED!! Also (much more) unstable!!
        %        Drift(:,2) = (param(6)*x(:,1) - 1/param(7)*x(:,2) - 1/param(8)*(exp(x(:,3))-1));
        %        Drift(:,3) = exp(-x(:,3)).*x(:,2); 
        %        Drift(:,4) = ( exp(x(:,3)-x(:,4)) - exp(x(:,4).*(param(10)-1)) )/param(9) ; %TRANSFORMED!!
        %        Drift(:,5) = ( exp(x(:,3)).*E_f - exp(x(:,4).*(param(10)-1)).*x(:,5) )/param(9); %TRANSFORMED!!
%           
%     % transformed HD eqs
%     s_trafo(:,t+1) = s_trafo(:,t) + (epsilon*z(:,t) - 1/tau_s*s_trafo(:,t) - 1/tau_f*(exp(g(:,t))-1))*h  ;
%     g(:,t+1) = g(:,t) + exp(-g(:,t))*s_trafo(:,t)*h ;
%     %trafo Balloon model
%     r(:,t+1) = r(:,t) + 1/tau_0*( exp(g(:,t)-r(:,t)) - exp(r(:,t)*(alpha-1)))*h ;
%         exponent = exp(-g(:,t));
%         E_f = ( 1 - (1-E_0).^exponent ) / E_0;
%     q_trafo(:,t+1) = q_trafo(:,t) + 1/tau_0*( exp(g(:,t)).*E_f - exp(r(:,t)*(alpha-1)).*q_trafo(:,t) )*h ;
%     %trafo BOLD signal
%     y_trafo(:,t+1) = V_0*(k1*(1-q_trafo(:,t+1)) + k2*(1-q_trafo(:,t+1).*exp(-r(:,t+1))) + k3*(1-exp(r(:,t+1)))) ;
     
    otherwise
        %% Lorenz system
        if strcmp(sys,'lorenz')
            Drift(:,1)=param(1)*(x(:,2)-x(:,1));
            Drift(:,2)=x(:,1).*(param(2)-x(:,3))-x(:,2);
            Drift(:,3)=x(:,1).*x(:,2)-param(3)*x(:,3);
        end
        
        %% 3D Constant Drift
        if strcmp(sys,'BM')
            
            Drift=repmat(param(1:dim),N,1);
            
        end
        
        if strcmp(sys,'L&S')
            
            Drift=5*x(2:end) + 100*x(2:end)./(1+x(2:end).^2) + 8*cos(x(1,1)*6/5);
            
        end
        
        if strcmp(sys,'NNfire')
            Inp = @(t,AmpInp,omega) AmpInp.*sin(omega*t/1000);
            transfer = @(A,theta,x, inp) tanh(A*x + theta + inp);
            
            Drift = -x + transfer(param(:,1:dim),repmat(param(:,dim+1),1,N),x',repmat(Inp(double(t),param(:,dim+2),param(:,dim+3)),1,N))';
        end
        %% Basket option dx = d(log(y))
        
        %         Drift=sigma-0.5*roh;
end
end