% Diffusion function g(t,X) for stochastic process dX = f(t,X)dt +
% g(t,X)*(u(t,X)dt + dW) where dW is m-dim Brownian motion with
% sigma=sqrt(dyn_var*h).

function diffusion = diffusion(X,sys)
[N,dim]=size(X);
diffusion=ones(N,dim);

switch sys
    case 'GMB'
        %% Geometric Brownian motion
        diffusion = X;
    case 'DCM'
        diffusion(:,3:5) = X(:,3:5);
    otherwise
        %Standard Brownian motion
        
end
