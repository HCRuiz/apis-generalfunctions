%% PRIOR in first iteration of controlled smoother; in higher iterations it is the inizialization, i.e. either prior or some other proposed distribution
% Samples from a Gaussian distribution using "type" to define the case
% (e.g. 'fixed', 'wide', 'adapt')

%INPUT:
%         mean : defines the center of the Gaussian: the first 
%         std : defines the stanard deviation 
%         N   : # of samples

function P=Prior(type,mean,std,N,dim_obs)
D=length(mean);
P=zeros(N,D);

switch type
       
    case 'fixed'
         %%Prior is observation+hidden variables given by mean OR initial
         %%condition w. mean=X0
            P(:,1:dim_obs)=mean(1:dim_obs);
            if D>dim_obs
                P(:,(dim_obs+1):end) = repmat(mean(dim_obs+1:end),N,1) + repmat(std(dim_obs+1:end),N,1).*randn(N,D-dim_obs);
            end
            disp('Prior fixed at observation; zeros in other dimensions')
        
    case 'wide'
         %%Wide Gaussian prior
            P=repmat(mean,N,1) + repmat(std,N,1).*randn(N,D);
            disp('Wide Gaussian prior')
        
    case 'adapt'
        %%Initialization according to a Gaussian proposal distribution with mean and variance obtained by the weighted particle system in APIS    
            P = repmat(mean,N,1) + repmat(std,N,1).*randn(N,D);
%             disp('Initialization is Gaussian with estimated smoothing stats')
            
    case 'adapt_mvn'
         %%Initialization according to a Gaussian proposal distribution with mean and covariance matrix obtained by the weighted particle system in APIS 
         P = mvnrnd(mean,std,N); %std here is SIGMA=covariance matrix
        disp('Initialization is MV Gaussian with estimated smoothing stats')
        
    case 'obs'
        %%Initialize around observation and with prior for hidden dimensions    
            auxN=10000;
            %Prior over states p(x)
            auxP = mean + std*randn(auxN,dim_obs);
            %Observation cost
            w = exp(-0.5*sum((repmat(mean(1:dim_obs),auxN,1)-auxP).^2./repmat(std(1:dim_obs),auxN,1).^2,2));
            w = w/sum(w);
            %STRUCTURAL RESAMPLING: Samples according to posterior p(x|y)
            K = ((1 : N) - rand)./N;
            bins=[0 cumsum(w')];
            [~,Idx]=histc(K,bins);
            
            P(:,1:dim_obs)=auxP(Idx,1);
            %still wide Gaussian prior for these two hidden dimensions
            P(:,2:D) = std*randn(N,D-1);
            disp('Initialization is p(x|y)~p(y|x)p(x); p(x): Gaussian N(0,var_prior)')
     
    otherwise
        error('Prior not specified')
end

        %% OLD CODE
% switch sys
%     
%     case 'BM'
%         %% Prior is initial condition
%         if strcmp(type,'fixed')
%             P(:,1:dim_obs)=mean;
%             P(:,(dim_obs+1):end)=zeros(N,D-dim_obs);
%             disp('Prior fixed at observation; zeros in other dimensions')
%         end
%         %% Wide Gaussian prior
%         if strcmp(type,'wide')
%             P=repmat(mean,N,1) + repmat(std,N,1).*randn(N,D);
%             disp('Wide Gaussian prior')
%         end
%         
%         %% Initialize around observation and with prior for hidden dimensions
%         if strcmp(type,'obs')
%             
%             auxN=10000;
%             %Prior over states p(x)
%             auxP = std*randn(auxN,dim_obs);
%             %Observation cost
%             w = exp(-0.5*sum((observ-auxP).^2,2)/var_obs);
%             w = w/sum(w);
%             %STRUCTURAL RESAMPLING: Samples according to posterior p(x|y)
%             K = ((1 : N) - rand)./N;
%             bins=[0 cumsum(w')];
%             [~,Idx]=histc(K,bins);
%             
%             P(:,1)=auxP(Idx,1);
%             %still wide Gaussian prior for these two hidden dimensions
%             P(:,2:D) = std*randn(N,D-1);
%             disp('Initialization is p(x|y)~p(y|x)p(x); p(x): Gaussian N(0,var_prior)')
%         end
%         %% Initialization according to a Gaussian proposal distribution with mean and variance obtained by the weighted particle system in APIS
%         if strcmp(type,'adapt')
%             
%             P = repmat(post0.mPS0,N,1) + repmat(post0.stdPS0,N,1).*randn(N,D);
%             disp('Initialization is Gaussian with estimated smoothing stats')
%         end
%         
%             
%     case 'NNfire'
%         if strcmp(type,'wide') %wide is not useful for APIS in the later iterations
%             P = repmat(mean,N,1) + repmat(std,N,1).*randn(N,D);
%             disp('Wide Gaussian Prior p(x)')
%         end
%         
%         if strcmp(type,'adapt')
%             P = repmat(post0.mPS0,N,1) + repmat(post0.stdPS0,N,1).*randn(N,D);
% %             disp('Initialization is Gaussian with estimated smoothing stats')
%         end
%          
%     case 'lorenz'
%        %% Prior is fixed at initial condition X0
%         if strcmp(type,'xo')
%             P=repmat(X0',N,1);
%             disp('Prior fixed at initial condition X0')
%         end
%         %% Prior is fixed at observation and open in hidden dimensions 
%         if strcmp(type,'fixed')
%             P(:,1)=observ;
%             P(:,2)=mean(2) + std*randn(N,1);
%             P(:,3)=mean(3) + std*randn(N,1);
%             disp('Fixed at observation; wide Gaussian y~N(0,7.5) and z~N(23,7.5) in hidden dimensions')
%         end
%         
%         %%Whenever considering a wide prior
%         %01/06/2014: after some simulations to obtain the mean and std of each of
%         %the components of the Lorenz system over time, I have to change the
%         %parameters from 7.5 to 5 for the std and for the mean of the z-comp from
%         %25 to 23
%         if strcmp(type,'wide') %wide is not useful for APIS
%             P = repmat(mean,N,1) + repmat(std,N,1).*randn(N,D);
%             disp('Wide Gaussian Prior p(x)')
%         end
%         
%         if strcmp(type,'obs')
%             
%             auxN=10000;
%             %Prior over states p(x)
%             auxP = mean(1) + std(1)*randn(auxN,dim_obs);
%             %Observation cost
%             w = exp(-0.5*sum((observ-auxP).^2,2)/var_obs);
%             w = w/sum(w);
%             %STRUCTURAL RESAMPLING: Samples according to posterior p(x|y)
%             K = ((1 : N) - rand)./N;
%             bins=[0 cumsum(w')];
%             [~,Idx]=histc(K,bins);
%             
%             P(:,1)=auxP(Idx);
%             %still q(x) for these two hidden dimensions
%             P(:,2)=mean(2) + std(2)*randn(N,1);
%             P(:,3)=mean(3) + std(3)*randn(N,1);
%             disp('Initialization is p(x|y)~p(y|x)p(x)')
%         end
%         
%         if strcmp(type,'adapt')
%            
%            P = repmat(post0.mPS0,N,1) + repmat(post0.stdPS0,N,1).*randn(N,D);
%             disp('Initialization is Gaussian with estimated smoothing stats')
%         end
%         
%         
%     case 'L&S'
%         if strcmp(type,'wide') %wide is not useful for APIS
%             P = repmat(mean,N,1) + repmat(std,N,1).*randn(N,D);
%             disp('Wide Gaussian Prior p(x)')
%         end
%         
%         if strcmp(type,'adapt')
%             
%             P = repmat(post0.mPS0,N,1) + repmat(post0.stdPS0,N,1).*randn(N,D);
%             disp('Initialization is Gaussian with estimated smoothing stats')
%         end
%               
%     otherwise
%         error('Prior not specified')
%         
% end