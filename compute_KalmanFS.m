%% Implements the KALMAN SMOOTHER for a Gaussian random walk (WITHOUT DRIFT!!)
% ONLY FOR OBSERVATIONS IN 1st DIMENSION
%% OUTPUT: Filter and smoother estimates
kalmanfilter=zeros(2,T); % contains the 1:mean and 2:variance of the kalman filter
kalmansmoother=zeros(2,T); % contains the 1:mean and 2:variance of the kalman smoother (RTS smoother)

%%Filter estimates
kalmanfilter(1,1)= mean_prior(1) + var_prior(1)*(Yx(1)- mean_prior(1))/(var_prior(1) + yvar);
kalmanfilter(2,1)= var_prior(1) - var_prior(1)^2/(var_prior(1) + yvar);
%Propagate
ot=2;
for t=2:T
    
    if ot<=length(obs) && t==obs(ot)
        %observation step
        kalmanfilter(1,obs(ot)) = kalmanfilter(1,t-1) + (kalmanfilter(2,t-1) + Q)*(Yx(ot) - kalmanfilter(1,t-1))/(kalmanfilter(2,t-1) + Q + yvar);
        kalmanfilter(2,obs(ot)) =  kalmanfilter(2,t-1) + Q - (kalmanfilter(2,t-1) + Q)^2/(kalmanfilter(2,t-1) + Q + yvar);
        ot=ot+1;
    else
        kalmanfilter(1,t) = kalmanfilter(1,t-1);
        kalmanfilter(2,t) = kalmanfilter(2,t-1) + Q;
    end
end

%End step
% kalmanfilter(1,obs(ot)) = kalmanfilter(1,t) + (kalmanfilter(2,t) + Q)*(Yx(ot) - kalmanfilter(1,t))/(kalmanfilter(2,t) + Q + yvar);
% kalmanfilter(2,obs(ot)) =  kalmanfilter(2,t) + Q - (kalmanfilter(2,t) + Q)^2/(kalmanfilter(2,t) + Q + yvar);

%%Smoother estimates
kalmansmoother(:,T)=kalmanfilter(:,T);
for t=T-1:-1:1
    kalmansmoother(1,t) = kalmanfilter(1,t) + kalmanfilter(2,t)*(kalmansmoother(1,t+1) - kalmanfilter(1,t))/(kalmanfilter(2,t) + Q);
    kalmansmoother(2,t) = kalmanfilter(2,t) + (kalmansmoother(2,t+1) - kalmanfilter(2,t) - Q)*(kalmanfilter(2,t)/(kalmanfilter(2,t) + Q)).^2;
end

stdkf = sqrt(kalmanfilter(2,:));
stdks= sqrt(kalmansmoother(2,:));

% % KALMAN SMOOTHER AND FILTERING
%     figure(1)
%     hold on
%     plot(taxis,kalmanfilter(1,:),'b',taxis,kalmansmoother(1,:),'r')
%     plot((obs-1)*h,Yx,'gd','MarkerFace','g')
%     plot(taxis,kalmanfilter(1,:)+stdkf,':b',taxis,kalmanfilter(1,:)-stdkf,':b')
%     plot(taxis,kalmansmoother(1,:)+stdks,':r',taxis,kalmansmoother(1,:)-stdks,':r')
%     hold off
%     legend('Kalman Filter','Kalman Smoother')
%     title('Exact solution: Kalman smoother')




