function logL = logLikelihood(y,Obs_Signal,param_obs,obs_model)

switch obs_model
    case 'Gauss'
        logL = - (y-Obs_Signal).^2/(2*param_obs); 
end