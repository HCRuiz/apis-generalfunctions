% Defines the signal used for the observations. It is NOT the observed noisy signal,
% rather, the underlaying signal that is used in the observation model
% defined in the logLikelihood.m function
function ObSignal = ObSignal(x,param_signal,signal_type)
switch signal_type
    case 'state_1'
        ObSignal = x(:,param_signal);
        
    case 'BOLD'
       % x = [z,s,f,v,q];
       % Param BOLD
       % param_signal = [V_0,k1,k2,k3]
%     BOLD = V_0*(k1*(1-x(:,5)) + k2*(1-x(:,5)./x(:,4)) + k3*(1-x(:,4))) ;
        ObSignal = param_signal(1)*(param_signal(2)*(1-x(:,5)) + param_signal(3)*(1-x(:,5)./x(:,4)) + param_signal(4)*(1-x(:,4))) ;
%         ObSignal = param_signal(1)*( param_signal(2)*(1-x(:,5)) + param_signal(3)*(1-x(:,5).*exp(-x(:,4))) + param_signal(4)*(1-exp(x(:,4))) ) ;

end