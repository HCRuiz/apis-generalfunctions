%% FFBSi 

%%FIRST PF then FFBSi
% Bootstrap Particle Filter
%Bootstrap in the sense that we take the proposal distribution given the
%observations to be the transition distribution (dyn. of the system), i.e.
%'uncontrolled' dynamics in the sense that we disregard the observations'

%The function has the option of giving a=1 as argument to enable
%resampling, otherwise resampling is left out.

%integration step of h=0.01 implements the dynamics by Euler method integration
%Observations made with observe.m

%INPUT

    % Nfb : # of particles
    % Yx : observations
    % obs: observation times
    % h : integration step (here:h=0.01)
    % T : total time
    % pri='fixed','wide' or 'obs' defines the prior
    if length(dyn_var)<D
    dyn_var = dyn_var*ones(1,D);
    end
%OUTPUT

    % w : weights over all observations. This is Nxlength(Yx) matrix
    % fbPS : particle paths NxTxD matrix with smoothing particles 
    % fbPF : particle paths NxTxD matrix with filtering particles 
    % BPS: backward perticle smoothing system is a NxTxD array containing backward
    %      trajectories

    %IF batch is needed
% mFPS=zeros(fbpsBatch,T,D);
% varFPS=zeros(fbpsBatch,T,D);
% meanBPS=zeros(fbpsBatch,T,D);
% varBPS=zeros(fbpsBatch,T,D);

%% FORWARD FILERING AND SMOOTHING PARTICLES (obtained by resampling)   

    %Initialize particles
    frPF=zeros(Nfb,T,D); %contains particle filter (without last step resampling)
    Filter=zeros(Nfb,T,D); %contains resampled particle filter
    FPS=zeros(Nfb,T,D); %contains smoother info
    w_pf=zeros(Nfb,nobs); % normalized weights of filter particle systems
    
    %Sample from prior : 'wide' is a broad prior over the whole system
    %                      'initial' is fixed point prior at initial condition X0
    %                      'flat' is uniformely distributed
    %                      'fixed' starts at the observation
    %For parameters see function Prior.m
    P=Prior(pri,mean_prior,std_prior,Nfb,dim_obs);
    frPF(:,1,:)=P;
    FPS(:,1,:)=frPF(:,1,:);
    
    %counts observations
    o=1;

    % Ctreate random process as a NxTxD matrix dW
    std_dyn(1,:,:) = sqrt(dyn_var*h);
    dW = bsxfun(@times,std_dyn,randn(Nfb,T,D));
    
 fprintf('Forward pass')
    for t=1:T
        %% RESAMPLING before propagating
        if o<=numel(obs) && t==obs(o);
            
            %Calculate and normalize weights
            w_pf(:,o) =  exp(-0.5*(frPF(:,t,1)-Yx(o)).^2/yvar);
            w_pf(:,o) = w_pf(:,o)./sum(w_pf(:,o));
            %STRUCTURAL RESAMPLING
            U = ((1 : Nfb) - rand)./Nfb;
            bins=[0 cumsum(w_pf(:,o))'];
            [~,Idx]=histc(U,bins);
            %%RESAMPLE
            FPS(:,1:t,:)=FPS(Idx,1:t,:); %kills and overwrites particle paths
            Filter(:,t,:)=frPF(Idx,t,:); %saves filter particles
            if t<T
             frPF(:,t,:)=frPF(Idx,t,:); %DO NOT resample the filter particles for BPS at the end
            end
            
            o=o+1;
             fprintf('.')
        end
        
        %% Propagate filtering particles
        if t<T
            squeezedPF=squeeze(frPF(:,t,:));
            frPF(:,t+1,:) = squeezedPF + Drift(t,squeezedPF,param,sys)*h + squeeze(dW(:,t,:));
            FPS(:,t+1,:) = frPF(:,t+1,:); 
            Filter(:,t+1,:) = frPF(:,t+1,:); 
        end
    end
    
    fprintf('\n')
    fprintf('Get FPS sample size for each time... \n')
    fpsESS = zeros(1,T);
    for t=1:T
        fpsESS(t) = size(unique(squeeze(FPS(:,t,:)),'rows'),1)/Nfb;
    end
    switch pri
        case 'fixed'
         fpsESS(1) = 1;   
    end
    
%% BACKWARD SAMPLING
    %   INPUT: PF is a NxTxD array and normalized weights w is a Nx(nobs)
    %   OUTPUT: Perticle smoothing system BPS is a NxTxD array containing backward
    %            trajectories
    BPS=zeros(M,T,D);
%     SM_Weights = zeros(M,T);
    
    %% sample particles PS(:,T,:) at end-time according to w_pf(:,T)
      %Calculate and normalize weights
            w_pf(:,end) =  exp(-0.5*(frPF(:,end,1)-Yx(end)).^2/yvar);
            w_pf(:,end) = w_pf(:,end)./sum(w_pf(:,end));
        %STRUCTURAL RESAMPLING
        U = ((1 : M) - rand)./M;
        bins=[0 cumsum(w_pf(:,end))'];
        [~,Idx]=histc(U,bins);
        BPS(:,end,:)=frPF(Idx,end,:);
        aux_weightvar = w_pf(Idx,end);
        
   %% BACKWARD PASS
   
    partSystem_backwards=squeeze(BPS(:,T,:));
    bESS=zeros(M,T);
    if Nfb==M
    ALL_BWeights_j = zeros(M,Nfb,T); 
    end
          fprintf('Backward pass...\n')
    for j=1:M
       particle_j=zeros(T,D);
       particle_j(T,:)=partSystem_backwards(j,:);
       bess_j=zeros(1,T);
       weights_forAll_t=ones(Nfb,T);
       weights_forAll_t(:,obs)=w_pf;
        bess_j(:,T)= 1/sum(weights_forAll_t(:,end).^2);
%        bsm_weights_j=zeros(1,T);
%        bsm_weights_j(T) = aux_weightvar(j);
%        
       auxfrPF=frPF;
       
        for t=T-1:-1:1
        %Drift of particles at time t
        squeezedPF=squeeze(auxfrPF(:,t,:));
        FX = squeezedPF + Drift(t,squeezedPF,param,sys)*h; 
                        
                Xfuture = repmat(particle_j(t+1,:),Nfb,1); % x_{t+1} for particle j
                w_bps = weights_forAll_t(:,t).*exp(-0.5*((Xfuture-FX).^2)*(1./(dyn_var'*h)));
                nw_bps = w_bps./sum(w_bps); % transition probability to all particles at time t
                bess_j(t)=1/sum(nw_bps.^2);
                weights_forAll_t(:,t) = nw_bps;
                
                %SAMPLING xt for particle j
                Idx=find(rand<cumsum(nw_bps),1);
                % Save backward step of particle j
                particle_j(t,:) = auxfrPF(Idx,t,:);
                % backward smoothing weights
%                 bsm_weights_j(t) = nw_bps(Idx).*bsm_weights_j(t+1);
   
        end
        bESS(j,:)=bess_j;
        BPS(j,:,:)=particle_j;
%         SM_Weights(j,:) = bsm_weights_j;
            if M==Nfb
                ALL_BWeights_j(j,:,:) = weights_forAll_t;
            end

    end

    if M==Nfb
        fprintf('Estimating smoothing weights \n')
        w_tT = zeros(Nfb,T);
        smESS = zeros(1,T);
        %all backward particles have the same ALL_BWeights matrix so pick
        %one randomly. This means that the weights w_T|T are the filter
        %weights 
        w_tT(:,T) = ALL_BWeights_j(randi(Nfb,1),:,T);
        smESS(T) = 1/sum(w_tT(:,T).^2);
        for t=T-1:-1:1
            
            w_tT(:,t) = w_tT(:,t+1)'*ALL_BWeights_j(:,:,t);
            smESS(t) = 1/sum(w_tT(:,t).^2);
            
        end
%             figure; plot(1:T,SM_Weights/Nfb)
    else
        fprintf('M not equal Nfb, estimation of smoothing weights is not possible! \n')
        smESS=nan;
    end

%% ESTIMATION OF SMOOTHING DISTRIBUTION MEAN
    mFPS = squeeze(mean(FPS));
    meanBPS = squeeze(mean(BPS));
    smESS = smESS/Nfb;
%% ESTIMATION OF SMOOTHING DISTRIBUTION VAR (DISABLED)
%(this estimation might be bias because estimating the variance from FPS and BSP is problematic due
% to the degeneracy and correlations between particles)
%     % From forward sampling-resampling
    varFPS = squeeze(var(FPS));
    % From backward pass
    varBPS = squeeze(var(BPS));
    

